import os
import dateparser
# from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys  # Added import statement
import django
import re
from decimal import Decimal

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Scan_Property.settings")
django.setup()
from home.models import Property


def get_link(url, query) -> list:
    # driver = Chrome()
    # driver.get(f'{url}q-{query.replace(" ", "-")}/?currency=EUR')

    # Find the search bar element
    # search_bar = driver.find_element(By.ID, 'headerSearch')
    # search_query = "garsoniera"  # Replace with your desired search query
    # search_bar.send_keys(search_query)
    # search_bar.send_keys(Keys.RETURN)  # Press Enter to perform the search

    adverts = []

    # properties = driver.find_elements(By.CSS_SELECTOR, '[data-cy="l-card"]')

    # for property in properties:
    title = property.find_element(By.TAG_NAME, "h6").get_attribute('innerText')
    price_element = property.find_element(By.CSS_SELECTOR, '[data-testid="ad-price"]')
    price = price_element.get_attribute('innerText')
    print(f'price={price}')

    # Extract numeric digits and decimal separator from the price string
    price_digits = re.findall(r'\d+|\.|,', price)
    price_digits = [item.replace(',', '.') for item in price_digits]
    price = Decimal(''.join(price_digits))

    currency = "€"
    city_and_date = property.find_element(By.CSS_SELECTOR, '[data-testid="location-date"]').get_attribute(
        'innerText')
    city, date_ = city_and_date.split(' - ')  # Split city_and_date into city and date
    print(f'city_and_date={city_and_date}')

    # Extract link to the object
    link = property.find_element(By.TAG_NAME, "a").get_attribute('href')
    print(f'link={link}')

    # Convert date string to a datetime object
    add_date = dateparser.parse(date_string=date_, date_formats=['%d %B %Y'], languages=['ro'])
    print(f'add_date={add_date}')

    print(f'title={title} type={type(title)}')
    print(f'price={price} type={type(price)}')
    print(f'currency={currency} type={type(currency)}')
    print(f'city={city} type={type(city)}')
    print(f'date={date_} type={type(date_)}')

    # Create a Property object and save it to the database
    property_record = Property.objects.create(title=title, price=price, currency=currency, city=city,
                                              added_date=add_date, year=None, link=link)
    adverts.append(property_record)

    # driver.quit()
    # return adverts


if __name__ == '__main__':
    print(get_link("https://www.olx.ro/imobiliare/", 'garsoniera'))
