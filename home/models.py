from django.db import models
from django.utils import html, timezone


# Create your models here.
class Member(models.Model):
        firstname = models.CharField(max_length=255)
        lastname = models.CharField(max_length=255)








class Property(models.Model):
    type = models.CharField(max_length=100)  # apartament,casa, garsoniera,etc.
    title = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=15, decimal_places=2)
    year = models.IntegerField(null=True, blank=True)
    added_date = models.DateField(default=timezone.now, null=True, blank=True)
    currency = models.CharField(max_length=10, choices=(('Lei', 'Lei'),('€', '€')))
    link = models.CharField(max_length=250,null=True,blank=True)

    def __str__(self):
        return f'{self.type}, with {self.title} in {self.city} {self.price}'

class Image(models.Model):
    description = models.TextField(max_length=100)
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    file = models.ImageField()

