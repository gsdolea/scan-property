from django.urls import path, include

from home import views
from home.views import property_filter

urlpatterns = [
    path('', views.home, name='home_page'),
    path('properties', property_filter, name='property_filter'),


]