# import selenium_olx
# from home.forms import PropertyFilterForm
#
#
# # Create your views here.
# def home(request):
#     # return HttpResponse('<h1>Welcome to yor new home finder </h1>')
#     return render(request, 'home_page.html')
#
# # def about(request):
# #     return render(request,'about.html')
#
# from django.shortcuts import render
#
# from .models import Property
#
#
# def property_filter(request):
#     form = PropertyFilterForm(request.GET)
#     print('running selenium script...')
#     selenium_olx.get_link()
#     # Apply filters based on the form data
#     properties = Property.objects.all()
#     if form.is_valid():
#         title = form.cleaned_data.get('title')
#         city = form.cleaned_data.get('city')
#         price = form.cleaned_data.get('price')
#         year = form.cleaned_data.get('year')
#         added_date = form.cleaned_data.get('added_date')
#         currency = form.cleaned_data.get('currency')
#         type = form.cleaned_data.get('type')
#
#         if title:
#             properties = properties.filter(title__icontains=title)
#         if city:
#             properties = properties.filter(city__icontains=city)
#         if price:
#             properties = properties.filter(price=price)
#         if year:
#             properties = properties.filter(year=year)
#         if added_date:
#             properties = properties.filter(added_date=added_date)
#         if currency:
#             properties = properties.filter(currency__iexact=currency)
#         if type:
#             properties = properties.filter(type=type)
#
#     context = {
#         'form': form,
#         'properties': properties
#     }
#
#     return render(request, 'property_filter.html', context)
#





# import selenium_olx
from django.shortcuts import render, redirect
from home.forms import PropertyFilterForm
from .models import Property

def home(request):
    return render(request, 'home_page.html')

def property_filter(request):
    form = PropertyFilterForm(request.GET)
    properties = Property.objects.all()

    if request.method == 'POST':
        query = request.POST.get('query', '')
        category = request.POST.get('category', 'https://www.olx.ro/imobiliare/')
        print('Running selenium script...')
        selenium_olx.get_link(category, query)
        return redirect('property_filter')  # Redirect back to the filter page after running the script

    if form.is_valid():
        # Apply filters based on the form data
        search_query = form.cleaned_data.get('search-query')
        # city = form.cleaned_data.get('city')
        # price = form.cleaned_data.get('price')
        # year = form.cleaned_data.get('year')
        # added_date = form.cleaned_data.get('added_date')
        # currency = form.cleaned_data.get('currency')
        # type_ = form.cleaned_data.get('type')

        if search_query:
            properties = properties.filter(title__icontains=search_query)
        # if city:
        #     properties = properties.filter(city__icontains=city)
        # if price:
        #     properties = properties.filter(price=price)
        # if year:
        #     properties = properties.filter(year=year)
        # if added_date:
        #     properties = properties.filter(added_date=added_date)
        # if currency:
        #     properties = properties.filter(currency__iexact=currency)
        # if type_:
        #     properties = properties.filter(type=type_)

    context = {
        'form': form,
        'properties': properties
    }

    return render(request, 'property_filter.html', context)
