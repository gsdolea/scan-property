from django import forms

class PropertyFilterForm(forms.Form):
    TYPES_CHOICES = (
        ('', 'Any'),
        ('house', 'House'),
        ('apartment', 'Apartment'),
        ('studio', 'Studio'),
    )

    title = forms.CharField(required=False)
    city = forms.CharField(required=False)
    price = forms.DecimalField(required=False)
    year = forms.IntegerField(required=False)
    added_date = forms.DateField(required=False)
    currency = forms.CharField(required=False)
    type = forms.ChoiceField(choices=TYPES_CHOICES, required=False)
    link= forms.CharField(required=False)