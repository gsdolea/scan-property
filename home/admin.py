from django.contrib import admin

from home.models import Property

# registration your models here.
admin.site.register(Property)